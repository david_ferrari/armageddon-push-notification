[Metodos](#markdown-header-metodos) •
[Preços](#markdown-header-preçosdia) •
[Extração Kibana](#markdown-header-extração-de-dados-kibana) •
[Documentação da firestore](#markdown-header-documentação-cloud-firebase) •
[Conclusão da Spike ACE-316](#markdown-header-conclusão-da-spike-ace-316)

---

# Metodos
 
1. addArmageddonMessage 
    * Adiciona mensagem no firestore
2. getArmageddonMessage
    * Retornar mensagem salva no firestore
3. getArmageddonMessageFromRemoteConfig 
    * Retorna mensagem situada no firebase RemoteConfigs

Todas funções não estão em sua versão definitiva necessitando de alterações para mitigar erros.

O que ainda necessita alterar, somente problemas conhecidos(possível existerem outros):
1. addArmageddonMessage: 
    * Adiciona mensagens mas não sobrescreve a primeira como é de es esperar o comportamento da implementação.
    * Pensar em outro nome para a collection que está atualmente como "messages".
    * Adicionar logs para facilitar debug.
2. getArmageddonMessage:
    * Está pegando o primeiro valor salvo na collection "messages".
    * Adição de alguns logs para um debug mais efetivo.
3. getArmageddonMessageFromRemoteConfig:
    * No emulator local consegue pegar as infos do remoteConfig, mas no deploy está tendo algum problema de permissão, [issue com mesmo problema citado](https://github.com/firebase/firebase-admin-node/issues/1015), trocar permissão para funcionar também no ambiente deployado.
    * Adicionar logs para debug.

# Preços/dia

|  Functions | Preço de banda firestore/Request  | Preço de chamada da função | Total |
| -------------------- | :----------: | :---------: | :----------: |
| addArmageddonMessage          |       US$ 0         |       US$ 0        |  US$ 0   |
| getArmageddonMessage             |         US$ 2,32       |       US$ 0,77    | US$ 3,09 |
| getArmageddonMessageFromRemoteConfig        |      0      |    US$ 0,77       | US$ 0,77 |

* ### Legenda:
    * Os preços são baseados num dia inteiro de uso das funções, é importante lembrar que nem sempre essas funções estarão em uso. 
    * A media de clientes usada para o calculo pode ser vista em: [extração kibana](#markdown-header-extracao-de-dados-kibana).
    * Os preços são calculados com base nas referências de preço e de estimativa da [documentação da firestore](#markdown-header-documentacao-cloud-firebase).


# Extração de dados Kibana
![data-extraction](./prints/month-requests.png)



### Consulta: [dados](https://kibana.kube.prod.easynvest.io/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-1y,to:now))&_a=(columns:!(),filters:!(),index:eeefa400-d465-11eb-a1e1-7d6e52cbda8b,interval:auto,query:(language:kuery,query:Armageddon),sort:!(!('@timestamp',desc))))

Esses dados representam 1 os dados de 1 ano para o dia atual(dia do print foi 20/07/2021), e a partir do mês 06/2021 onde houve alto uso da API Armageddon foi tirada uma média de **3927419.5 Requests por dia**, 3 milhões 927 mil e 419,5 requets.

# Documentação Cloud Firebase

[Estimativa de custo de banda do firestore em aplicações](https://firebase.google.com/docs/firestore/billing-example#small-50k-installs)

[Guia para uso do "monitor" do firebase para verificação de uso das funções e health](https://firebase.google.com/docs/firestore/monitor-usage)

[Estimativas do tamanho dos tipos de dado armazenados no firestore](https://firebase.google.com/docs/firestore/storage-size)

[Tabela de preços](https://firebase.google.com/pricing) 


# Conclusão da Spike ACE-316

O custo para utilização das functions com storage no firebase é muito maior do que a utilização somente das functions lendo o remote config que é gratuito, sendo também necessário apenas implementação de uma função e portanto manutenção de uma só função.

Quanto a usuabilidade a [documentação](https://firebase.google.com/docs/remote-config/use-cases) do firebase descreve como pode ser personalizada as configurações para atender diferentes configurações de disparo.

Observando algumas utilizações de outras pessoas(stackoverflow, reddit, posts no medium) existem casos semelhantes ao problema que estamos tentando resolver sendo utilizado o remoteConfig como solução, inclusive algumas mensagens no próprio firebase da easy já seta algumas mensagens como login_username_helper, login_disableduser_title, askto_upgrade_msg, entre outras que são variáveis que guardam somente uma mensagem.