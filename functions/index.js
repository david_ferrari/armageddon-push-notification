const functions = require("firebase-functions");
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();

exports.addArmageddonMessage = functions.https.onRequest(async (req, res) => {
    const original = req.query.text;
    const writeResult = await admin.firestore().collection('messages').add({original: original});
    console.log(req.query.text);
    res.json({result: `Message with ID: ${writeResult.Id} added.`});
});

exports.getArmageddonMessage = functions.https.onRequest(async (req, res) => {

  db.collection("messages").get().then((querySnapshot) => {
    if(querySnapshot.docs.length == 0)
      res.json({message: "Armageddon message without value"});
    else{
      const doc = querySnapshot.docs[0].data();
      res.json({message: `${doc.original}`});
    }
  });

});


exports.getArmageddonMessageFromRemoteConfig = functions.https.onRequest(async (req, res) => {
    const config = await admin.remoteConfig();
    config.getTemplate()
      .then(function (template) {

        //print specific value
        const armageddonMessage = template.parameters.test_armageddon_message.defaultValue.value;
        console.log(armageddonMessage);

        //print all values from remoteConfig for debugging
        // var templateStr = JSON.stringify(template);
        // console.log(templateStr);
        res.json({message: `${armageddonMessage}`});
      })
      .catch(function (err) {
        console.error('Unable to get template');
        console.error(err);
    });
})
